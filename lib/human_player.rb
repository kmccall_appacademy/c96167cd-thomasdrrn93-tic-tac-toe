class HumanPlayer

  attr_accessor :mark, :board 
  attr_reader :name

  def initialize(name)
    @name = name
  end

  def get_move
    puts "Where would you like to move? (row, col)"
    gets.chomp.split(",").map(&:to_i)
  end

  def display(board)
    display_board = board.grid
   top_row = display_board[0].map { |el| el == nil ? el = " " : el = el.to_s }
   middle_row = display_board[1].map { |el| el == nil ? el = " " : el = el.to_s }
   bottom_row = display_board[2].map { |el| el == nil ? el = " " : el = el.to_s }
   puts "\n#{top_row}\n\n#{middle_row}\n\n#{bottom_row}"
  end
end
