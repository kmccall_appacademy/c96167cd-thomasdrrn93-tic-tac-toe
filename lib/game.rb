require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game

  attr_accessor :board, :current_player, :player1, :player2, :human_player 

  def initialize(player1, player2)
    @player1 = player1
    @player2 = player2
    @board = Board.new
    @current_player = player1
    player1.mark = :X
    player2.mark = :O
  end

  def play_turn
    board.place_mark(current_player.get_move, current_player.mark)
    self.switch_players!
  end

  def switch_players!
    if @current_player == player1
      @current_player = player2
    else
      @current_player = player1
    end
  end

  def play
    until board.winner != nil
    puts current_player.display(board)
    play_turn
    end
    puts current_player.display(board)
  end
end

if __FILE__ == $PROGRAM_NAME
  print "Enter your name: "
  name = gets.chomp.strip
  human = HumanPlayer.new(name)
  ai = ComputerPlayer.new('AI')

  new_game = Game.new(human, ai)
  new_game.play
end
