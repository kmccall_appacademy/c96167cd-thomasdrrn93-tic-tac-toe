class Board

  attr_reader :grid, :marks

  def initialize(grid = [[nil, nil, nil], [nil, nil, nil], [nil, nil, nil]])
    @grid = grid
    @marks = [:X, :O]
  end

  def place_mark(pos, mark)
    idx1 = pos[0]
    idx2 = pos[1]
    self.grid[idx1][idx2] = mark
  end

  def empty?(pos)
    idx1 = pos[0]
    idx2 = pos[1]
    self.grid[idx1][idx2] == nil
  end

  def winner
    return :X if self.winrowx?
    return :X if self.windiagx?
    return :X if self.wincolumnx?
    return :O if self.winrowo?
    return :O if self.windiago?
    return :O if self.wincolumno?
  end

  def winrowx?
    self.grid.any? do |row|
      row.all? { |sym| sym == :X }
    end
  end

  def winrowo?
    self.grid.any? do |row|
      row.all? { |sym| sym == :O }
    end
  end

  def windiagx?
    left = []
    right = []
    l = (self.grid.length) - 1
    idx = 0
    until l < 0
      left << self.grid[idx][idx]
      right << self.grid[idx][l]
      idx += 1
      l -= 1
    end
    left.all? { |sym| sym == :X } || right.all? { |sym| sym == :X }
  end

  def windiago?
    left = []
    right = []
    l = (self.grid.length) - 1
    idx = 0
    until l < 0
      left << self.grid[idx][idx]
      right << self.grid[idx][l]
      idx += 1
      l -= 1
    end
    left.all? { |sym| sym == :O } || right.all? { |sym| sym == :O }
  end

  def wincolumnx?
    column = []
    idx1 = 0
    idx2 = (self.grid.length) - 1
    until idx1 > idx2
      column << self.grid[idx1][idx2]
      idx1 += 1
    end
    column.all? { |sym| sym == :X }
  end

  def wincolumno?
    column = []
    idx1 = 0
    idx2 = (self.grid.length) - 1
    until idx1 > idx2
      column << self.grid[idx1][idx2]
      idx1 += 1
    end
    column.all? { |sym| sym == :O }
  end

  def tie?
    self.grid.all? do |row|
      row.all? { |sym| sym != nil }
    end
  end

  def over?
    if self.winner != nil || self.tie?
      return true
    else
      return false
    end
  end
end
