class ComputerPlayer

  attr_accessor :mark
  attr_reader :name, :board 

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    potential_moves = []
    winning_move = nil
    (0..2).each do |row|
      (0..2).each do |col|
        potential_moves << [row, col] if board.empty?([row, col])
      end
    end
    potential_moves.each do |move|
      board.place_mark(move, mark)
      winning_move = move if board.winner
      board.place_mark(move, nil)
    end

    return winning_move if winning_move
    potential_moves.sample

  end
end
